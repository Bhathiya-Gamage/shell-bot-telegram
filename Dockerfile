FROM ubuntu:20.04

# Add user

WORKDIR /root
# Install some tools
RUN apt-get update && apt install ca-certificates publicsuffix libapt-pkg6.0 libpsl5 libssl1.1 libnss3 openssl wget locales bzip2 tzdata sudo make python build-essential npm -y && apt install git -y 
RUN git clone https://gitlab.com/Bhathiya-Gamage/shell-bot-telegram 
RUN cd shell-bot-telegram && npm install
CMD cd shell-bot-telegram && node server
